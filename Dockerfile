FROM node:14
ADD source/. source/.
WORKDIR /source
CMD ["npm" , "install"]
CMD ["npm" , "start"]
# Expose ports.
EXPOSE 80
EXPOSE 443
EXPOSE 4200
EXPOSE 4567
EXPOSE 430
